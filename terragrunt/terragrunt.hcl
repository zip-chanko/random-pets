remote_state {
  backend = "s3"
  config = {
    bucket                 = "terragrunt-terraform"
    key                    = "${path_relative_to_include()}/${get_env("ENV")}.tfstate"
    skip_bucket_versioning = true
    encrypt                = true
    region                 = "ap-southeast-2"
  }
}

terraform {
  extra_arguments "tf_env_vars" {
    commands = [get_terraform_command()]
    env_vars = {
      TF_PLUGIN_CACHE_DIR = "${get_repo_root()}/.terraform.d/_plugins"
      TF_DATA_DIR         = "${get_repo_root()}/.terraform.d/${get_env("ENV")}/${path_relative_to_include()}"
    }
  }

  before_hook "create_internal_dirs" {
    commands = ["init"]
    execute  = ["mkdir", "-p", "${get_repo_root()}/.terraform.d/_plugins"]
  }
}
