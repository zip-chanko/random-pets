include "root" {
  path = find_in_parent_folders()
}

include "env_dev" {
  path = find_in_parent_folders("_dev.hcl")
}

include "env_sand" {
  path = find_in_parent_folders("_sand.hcl")
}

include "env_prod" {
  path = find_in_parent_folders("_prod.hcl")
}

terraform {
  source = "${get_repo_root()}/terraform//pets"
}

dependencies {
  paths = ["${get_repo_root()}/terragrunt/dogs", "${get_repo_root()}/terragrunt/cats"]
}

dependency "dogs" {
  config_path = "${get_repo_root()}/terragrunt/dogs"

  mock_outputs = {
    dogs = ["woof"]
  }

  mock_outputs_merge_strategy_with_state = "shallow"
}

dependency "cats" {
  config_path = "${get_repo_root()}/terragrunt/cats"

  mock_outputs = {
    cats = ["meow"]
  }

  mock_outputs_merge_strategy_with_state = "shallow"
}

inputs = {
  dogs = dependency.dogs.outputs.dogs
  cats = dependency.cats.outputs.cats
}
