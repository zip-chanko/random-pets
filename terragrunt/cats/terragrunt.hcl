include "root" {
  path = find_in_parent_folders()
}

include "env_dev" {
  path = find_in_parent_folders("_dev.hcl")
}

include "env_sand" {
  path = find_in_parent_folders("_sand.hcl")
}

include "env_prod" {
  path = find_in_parent_folders("_prod.hcl")
}

terraform {
  source = "${get_repo_root()}/terraform//cats"
}

inputs = {
  number = 2
}
