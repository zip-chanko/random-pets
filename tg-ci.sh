#!/usr/bin/env bash

# CAUTION!
# This script uses long option which is not supported in builtin getopt command on macOS.
# Please consider installing gnu-getopt.

set -e

# Function to show help
function usage {
    echo
    echo "Usage: $(basename "$0") [OPTIONS]"
    echo
    echo "Options:"
    echo "  -c    Terragrunt command (e.g. -c plan, -c 'run-all plan')"
    echo "  -d    Terragrunt directory"
    echo "  -h    Show this help message"
    exit 2
}

# Set command line options
while getopts 'c:d:h' opt; do
  case "$opt" in
    c) 
      tg_command+=("$OPTARG")
      ;;
    d) 
      tg_dir="$OPTARG"
      ;;
    :) 
      usage
      ;;
    ?|h) 
      usage
      ;;
  esac
done
shift "$(($OPTIND -1))"


# write log message with timestamp
function log {
  local -r message="$1"
  local -r timestamp=$(date +"%Y-%m-%d %H:%M:%S")
  >&2 echo -e "${timestamp} ${message}"
}

# remove ANSI color codes from argument variable
function clean_colors {
  local -r input="$1"
  echo "${input}" | sed -E 's/\x1B\[[0-9;]*[mGK]//g'
}

# run terragrunt commands in specified directory
# arguments: directory and terragrunt command
# terragrunt_log_file path to log file
# terragrunt_exit_code exit code of terragrunt command
function run_terragrunt {
  local -r dir="$1"
  local -r command=($2)

  # terragrunt_log_file can be used later as file with execution output
  terragrunt_log_file=$(mktemp)

  log "Running Terragrunt in ${dir}"
  terragrunt "${command[@]}" --terragrunt-working-dir "${dir}" 2>&1 | tee "${terragrunt_log_file}"
  # terragrunt_exit_code can be used later to determine if execution was successful
  terragrunt_exit_code=${PIPESTATUS[0]}
}

# post comment to pull request
function comment {
  local -r message="$1"
  if [[ -z "${CI_MERGE_REQUEST_PROJECT_ID}" || -z $CI_MERGE_REQUEST_IID ]]; then
    log "Skipping comment as merge request ID is missing."
    return
  fi
  local comment_url
  comment_url="$CI_API_V4_URL/projects/$CI_MERGE_REQUEST_PROJECT_ID/merge_requests/$CI_MERGE_REQUEST_IID/notes"
  local messagePayload
  messagePayload=$(jq -n --arg body "$message" '{ "body": $body }')
  curl -s -S -H "PRIVATE-TOKEN: $GITLAB_TOKEN" -H "Content-Type: application/json" -d "$messagePayload" "$comment_url"
}

# find changes
function changes {
  local -r output="$1"
  # Initialize variables
  to_add=0
  to_change=0
  to_destroy=0

  # Process each line in the terragrunt_output
  while read -r line; do
    # Check if the line matches the expected pattern
    if [[ $line =~ Plan:\ ([0-9]+)\ to\ add,\ ([0-9]+)\ to\ change,\ ([0-9]+)\ to\ destroy\. ]]; then
      # Extract the numbers using the regex capture groups
      current_to_add="${BASH_REMATCH[1]}"
      current_to_change="${BASH_REMATCH[2]}"
      current_to_destroy="${BASH_REMATCH[3]}"
      
      # Add the extracted numbers to the corresponding variables
      to_add=$((to_add + current_to_add))
      to_change=$((to_change + current_to_change))
      to_destroy=$((to_destroy + current_to_destroy))
    fi
  done <<< "$output"
}

function main {
  log "Starting Terragrunt run"
  trap 'log "Finished Terragrunt execution"' EXIT

  # Command check
  if [ -z "${tg_command}" ]; then
    log "command option is missing."
    usage
  fi

  # Run in same directory if -d is not provided
  if [ -z "${tg_dir}" ]; then
    tg_dir=$(pwd)
  fi

  # add auto approve for apply and destroy commands
  if [[ "$tg_command" == "apply"* || "$tg_command" == "destroy"* || "$tg_command" == "run-all apply"* || "$tg_command" == "run-all destroy"* ]]; then
    local -r tg_arg_and_commands="${tg_command} -auto-approve --terragrunt-non-interactive"
  else
    local -r tg_arg_and_commands="${tg_command}"
  fi
  run_terragrunt "${tg_dir}" "${tg_arg_and_commands}"

  local -r log_file="${terragrunt_log_file}"
  trap 'rm -rf ${log_file}' EXIT

  local exit_code
  exit_code=$(("${terragrunt_exit_code}"))

  local terragrunt_log_content
  terragrunt_log_content=$(cat "${log_file}")
  # output without colors
  local terragrunt_output
  terragrunt_output=$(clean_colors "${terragrunt_log_content}")

  # comment execution result
  if [[ "${CI_PIPELINE_SOURCE}" == "merge_request_event" && "$tg_command" == "plan"* || "$tg_command" == "run-all plan"* ]]; then
    # find changes
    changes "${terragrunt_output}"

    comment "Execution result of \`$tg_command\` for \`$ENV\` environment : $to_add to add, $to_change to change, $to_destroy to destroy.
<details><summary>Result</summary>

\`\`\`\hcl
${terragrunt_output}
\`\`\`

</details>
Job id [$CI_JOB_ID]($CI_JOB_URL).
    "
  fi

  exit $exit_code
}

main "$@"
