provider "random" {}

terraform {
  backend "s3" {}
}

variable "number" {
  type    = number
  default = 1
}

resource "random_pet" "dog" {
  count = var.number

  prefix = "dog"
}

output "dogs" {
  value = random_pet.dog.*.id
}
