provider "random" {}

terraform {
  backend "s3" {}
}

variable "number" {
  type    = number
  default = 1
}

resource "random_pet" "cat" {
  count = var.number

  prefix = "cat"
}

output "cats" {
  value = random_pet.cat.*.id
}
