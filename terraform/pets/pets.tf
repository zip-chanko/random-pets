variable "dogs" {
  type = list(string)
}

variable "cats" {
  type = list(string)
}

terraform {
  backend "s3" {}
}

locals {
  pets = concat(var.dogs, var.cats)
}

resource "random_id" "id" {
  byte_length = length(local.pets)
}

output "pets" {
  value = local.pets
}
